package com.example.chatmodule.common.network

import com.example.chatmodule.models.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI

object Connection {
    const val urlWS = "ws://strukov-artemii.online:8085/"
    const val urlHttp = "http://strukov-artemii.online:8085/"

    private val callbacks: MutableList<CallbackWebSocketClient> = mutableListOf()
    fun addWebSocketCallback(callbackWebSocketClient: CallbackWebSocketClient){
        callbacks.add(callbackWebSocketClient)
    }

    val client = object: WebSocketClient(URI("$urlWS/chat"), mapOf("idUser" to Info.idUser.toString())) {
        override fun onOpen(handshakedata: ServerHandshake?) {
            callbacks.forEach {
                it.onOpen()
            }
        }

        override fun onMessage(message: String) {
            val modelType = Gson().fromJson(message, ModelType::class.java)
            val model: Any = when(modelType.type){
                "message" -> Gson().fromJson<ModelAnswerChat<ModelMessage>>(message, object: TypeToken<ModelAnswerChat<ModelMessage>>(){}.type).body
                "chat" -> Gson().fromJson<ModelAnswerChat<ModelChat>>(message, object: TypeToken<ModelAnswerChat<ModelChat>>(){}.type).body
                "chats" -> Gson().fromJson<ModelAnswerChat<List<ModelDataChat>>>(message,object: TypeToken<ModelAnswerChat<List<ModelDataChat>>>(){}.type).body
                "audio" -> Gson().fromJson<ModelAnswerChat<List<Int>>>(message, object: TypeToken<ModelAnswerChat<List<Int>>>(){}.type).body
                else -> Gson().fromJson<ModelAnswerChat<ModelUser>>(message, object: TypeToken<ModelAnswerChat<ModelUser>>(){}.type).body
            }
            when(model){
                is ModelUser -> {
                    callbacks.forEach {
                        it.onPersonData(model)
                    }
                }
                is ModelMessage -> {
                    callbacks.forEach{
                        it.onMessage(model)
                    }
                }
                is ModelChat -> {
                    callbacks.forEach{
                        it.onChat(model)
                    }
                }
                is List<*> -> {
                    if (model[0] is ModelDataChat) {
                        callbacks.forEach{
                            it.onChats(model.map { elem -> elem as ModelDataChat })
                        }
                    }
                }
            }
        }

        override fun onClose(code: Int, reason: String?, remote: Boolean) {}

        override fun onError(ex: Exception?) {}
    }

    fun removeWebSocketCallback(callbackWebSocketClient: CallbackWebSocketClient){
        callbacks.remove(callbackWebSocketClient)
    }
}

interface CallbackWebSocketClient{
    fun onMessage(modelMessage: ModelMessage)

    fun onChat(modelChat: ModelChat)

    fun onChats(chats: List<ModelDataChat>)

    fun onPersonData(modelUser: ModelUser)

    fun onOpen()
}
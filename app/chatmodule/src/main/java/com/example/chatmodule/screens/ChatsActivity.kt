package com.example.chatmodule.screens

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatmodule.common.network.CallbackWebSocketClient
import com.example.chatmodule.common.network.Connection
import com.example.chatmodule.common.adapters.ChatsAdapter
import com.example.chatmodule.common.adapters.OnClickChat
import com.example.chatmodule.databinding.ActivityChatsBinding
import com.example.chatmodule.models.ModelChat
import com.example.chatmodule.models.ModelDataChat
import com.example.chatmodule.models.ModelMessage
import com.example.chatmodule.models.ModelUser
import com.google.gson.Gson

class ChatsActivity : AppCompatActivity(), CallbackWebSocketClient {

    private lateinit var binding: ActivityChatsBinding
    private lateinit var adapter: ChatsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Connection.addWebSocketCallback(this)
        Connection.client.connect()

        adapter = ChatsAdapter(onClickChat = object: OnClickChat {
            override fun onClickChat(dataChat: ModelDataChat) {
                val intent = Intent(this@ChatsActivity, ChatActivity::class.java)
                val jsonDataChat = Gson().toJson(dataChat)
                intent.putExtra("jsonDataChat", jsonDataChat)
                startActivity(intent)
            }
        })

        binding.chats.apply {
            layoutManager = LinearLayoutManager(this@ChatsActivity)
            adapter = this@ChatsActivity.adapter
        }
    }

    override fun onMessage(modelMessage: ModelMessage) {}

    override fun onChat(modelChat: ModelChat) {}

    override fun onChats(chats: List<ModelDataChat>) {
        runOnUiThread {
            adapter.setData(chats)
        }
    }

    override fun onPersonData(modelUser: ModelUser) {
        runOnUiThread {
            binding.nickname.text = modelUser.lastname[0].toString()
            binding.cardView.setCardBackgroundColor(modelUser.getColorCard())
        }
    }

    override fun onOpen() {
        Connection.client.send("/chats")
    }
}

fun showAlertDialog(
    context: Context,
    title: String,
    message: String
){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("Повторить еще раз", null)
        .show()
}

fun showError(context: Context, error: String){
    showAlertDialog(context, "Ошибка", error)
}
package com.example.chatmodule.screens

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatmodule.*
import com.example.chatmodule.common.adapters.ChatAdapter
import com.example.chatmodule.common.network.CallbackWebSocketClient
import com.example.chatmodule.common.network.Connection
import com.example.chatmodule.common.network.Info.idUser
import com.example.chatmodule.databinding.ActivityChatBinding
import com.example.chatmodule.models.*
import com.google.gson.Gson


class ChatActivity: AppCompatActivity(), CallbackWebSocketClient, OnAudioMessage {

    private lateinit var binding: ActivityChatBinding

    private lateinit var adapter: ChatAdapter
    private lateinit var chat: ModelDataChat

    private var mediaPlayer: MediaPlayer? = null

    private var nowRecord: Boolean = false
    private var preparingSend: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        chat = Gson().fromJson(intent.getStringExtra("jsonDataChat"), ModelDataChat::class.java)

        adapter = ChatAdapter(mutableListOf(), this)
        binding.recChat.apply {
            layoutManager = LinearLayoutManager(this@ChatActivity, LinearLayoutManager.VERTICAL, true)
            adapter = this@ChatActivity.adapter
        }
        binding.back.setOnClickListener {
            finish()
        }

        loadHistory()
        fillInfoChat()

        Connection.addWebSocketCallback(object: CallbackWebSocketClient{
            override fun onMessage(modelMessage: ModelMessage) {

            }

            override fun onChat(modelChat: ModelChat) {

            }

            override fun onChats(chats: List<ModelDataChat>) {

            }

            override fun onPersonData(modelUser: ModelUser) {

            }

            override fun onOpen() {

            }
        })

        binding.send.setOnClickListener {
            if (nowRecord){
                nowRecord = false
                preparingSend = true
                showSendInfo()
            }else if (!preparingSend){
                val modelSendMessage = SendMessage(binding.messageText.text.toString(), chat.id, false)
                Connection.client.send(Gson().toJson(modelSendMessage))
                binding.messageText.setText("")
            }
        }

        binding.playPause.setOnClickListener {
            if (mediaPlayer != null){
                if (mediaPlayer!!.isPlaying){
                    binding.playPause.setImageResource(R.drawable.play)
                    mediaPlayer!!.pause()
                }else {
                    binding.playPause.setImageResource(R.drawable.pause)
                    mediaPlayer!!.start()
                }
            }
        }

        binding.mic.setOnClickListener {
            if (nowRecord || preparingSend){
                nowRecord = false
                preparingSend = false
                showNormal()
            }else{
                nowRecord = true
                showRecordInfo()
            }
        }

        binding.closePlayer.setOnClickListener {
            if (mediaPlayer != null){
                stopAndHide()
            }
        }
    }

    private fun showRecordInfo(){
        binding.mic.setImageResource(R.drawable.baseline_close_24)
        binding.linAudioInfo.setHorizontalGravity(Gravity.CENTER)
        binding.messageText.visibility = View.GONE
        binding.linAudioInfo.visibility = View.VISIBLE
        binding.recordDot.visibility = View.VISIBLE
    }

    private fun showSendInfo(){
        binding.recordDot.visibility = View.GONE
        binding.lostTimeSend.visibility = View.VISIBLE
        binding.progressBar.visibility = View.VISIBLE
        binding.send.visibility = View.INVISIBLE
        binding.linAudioInfo.setHorizontalGravity(Gravity.LEFT)
    }

    private fun showNormal(){
        binding.mic.setImageResource(R.drawable.baseline_mic_24)
        binding.linAudioInfo.visibility = View.GONE
        binding.messageText.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
        binding.send.visibility = View.VISIBLE
        binding.lostTimeSend.visibility = View.GONE

    }

    override fun onDestroy() {
        Connection.removeWebSocketCallback(this)
        mediaPlayer?.stop()
        mediaPlayer = null
        super.onDestroy()
    }

    private fun fillInfoChat(){
        val modelUser =chat.getOtherUser(idUser)
        binding.fi.text = modelUser.getFI()
        binding.chatCoverText.text = modelUser.lastname[0].toString()
        binding.cardChat.setCardBackgroundColor(modelUser.getColorCard())
    }

    private fun loadHistory() {
        Connection.client.send("/chat ${chat.id}")
    }

    private fun getUser(idUser: Int): ModelUser {
        if (idUser == chat.first.id) return chat.first
        if (idUser == chat.second.id) return chat.second
        throw Exception()
    }

    private fun showToast(message: String){
        runOnUiThread { Toast.makeText(this, message, Toast.LENGTH_LONG).show() }
    }

    override fun onMessage(modelMessage: ModelMessage) {
        if (modelMessage.idChat == chat.id) {
            if (modelMessage.idUser == null) {
                showToast(modelMessage.message)
            } else {
                runOnUiThread {
                    val user = getUser(modelMessage.idUser)
                    adapter.addMessage(modelMessage.toModelMessageAdapter(user))
                    binding.recChat.scrollToPosition(0)

                    if (binding.emptyText.visibility == View.VISIBLE)
                        binding.emptyText.visibility = View.GONE
                }
            }
        }
    }

    override fun onChat(modelChat: ModelChat) {
        val messagesAdapter = modelChat.messages.map {
            it.toModelMessageAdapter(getUser(it.idUser), it.idUser == idUser)
        }.reversed()
        if (modelChat.messages.isEmpty()){
            runOnUiThread {
                binding.emptyText.visibility = View.VISIBLE
            }
        }else{
            runOnUiThread {
                adapter.setMessages(messagesAdapter)
                binding.emptyText.visibility = View.GONE
            }
        }
    }

    override fun onChats(chats: List<ModelDataChat>) {}

    override fun onPersonData(modelUser: ModelUser) {}


    override fun onOpen() {}

    override fun onPlayMessage(modelMessageAdapter: ModelMessageAdapter) {
        binding.linPlayer.visibility = View.VISIBLE
        mediaPlayer = MediaPlayer()
        mediaPlayer!!.setDataSource("${Connection.urlHttp}/media/${modelMessageAdapter.id}")
        mediaPlayer!!.setOnPreparedListener {
            binding.playPause.setImageResource(R.drawable.pause)
            mediaPlayer = it
            mediaPlayer!!.start()
            binding.seekBar.max = mediaPlayer!!.duration
            val mHandler = Handler(Looper.getMainLooper())
            this@ChatActivity.runOnUiThread(object : Runnable {
                override fun run() {
                    if (mediaPlayer != null) {
                        val mCurrentPosition: Int = mediaPlayer!!.currentPosition
                        binding.seekBar.progress = mCurrentPosition
                        mHandler.postDelayed(this, 1000)
                    }else{
                        stopAndHide()
                    }
                }
            })
        }
        mediaPlayer!!.setOnCompletionListener {
            stopAndHide()
        }
        mediaPlayer!!.prepareAsync()
    }

    fun stopAndHide(){
        binding.playPause.setImageResource(R.drawable.play)
        binding.seekBar.progress = 0
        mediaPlayer?.stop()
        mediaPlayer = null
        binding.linPlayer.visibility = View.GONE
    }
}

interface OnAudioMessage{
    fun onPlayMessage(modelMessageAdapter: ModelMessageAdapter)
}
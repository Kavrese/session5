package com.example.chatmodule.common.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chatmodule.models.ModelMessageAdapter
import com.example.chatmodule.models.ModelUser.Companion.calcColorAlphaForIdUser
import com.example.chatmodule.databinding.ItemAudioMessageMyBinding
import com.example.chatmodule.databinding.ItemMessageMyBinding
import com.example.chatmodule.databinding.ItemMessageOtherBinding
import com.example.chatmodule.common.network.Info
import com.example.chatmodule.screens.OnAudioMessage

class ChatAdapter(
    private val messages: MutableList<ModelMessageAdapter>,
    private val onAudioMessage: OnAudioMessage
): RecyclerView.Adapter<ChatAdapter.VH>() {
    open class VH(itemView: View): RecyclerView.ViewHolder(itemView){
        open fun bind(modelMessage: ModelMessageAdapter){}
    }

    class MyVH(private val binding: ItemMessageMyBinding): VH(binding.root){
        override fun bind(modelMessage: ModelMessageAdapter) {
            binding.message.text = modelMessage.message
            binding.datetime.text = modelMessage.datetime
            binding.backgroundMessage.setBackgroundColor(Info.idUser.calcColorAlphaForIdUser())
        }
    }

    class OtherVH(private val binding: ItemMessageOtherBinding): VH(binding.root){
        override fun bind(modelMessage: ModelMessageAdapter) {
            binding.message.text = modelMessage.message
            binding.datetime.text = modelMessage.datetime
            binding.backgroundMessage.setBackgroundColor(modelMessage.user.getColorAlphaCard())
        }
    }

    class AudioMyVH(private val binding: ItemAudioMessageMyBinding): VH(binding.root){
        override fun bind(modelMessage: ModelMessageAdapter) {
            binding.datetime.text = modelMessage.datetime
            binding.backgroundMessage.setBackgroundColor(Info.idUser.calcColorAlphaForIdUser())
        }
    }

    class AudioOtherVH(private val binding: ItemAudioMessageMyBinding): VH(binding.root){
        override fun bind(modelMessage: ModelMessageAdapter) {
            binding.datetime.text = modelMessage.datetime
            binding.backgroundMessage.setBackgroundColor(modelMessage.user.getColorAlphaCard())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return when(viewType){
            0 -> MyVH(ItemMessageMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            1 -> OtherVH(ItemMessageOtherBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            2 -> AudioMyVH(ItemAudioMessageMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> AudioOtherVH(ItemAudioMessageMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(messages[position])
        if (getItemViewType(position) > 1){
            // Аудио сообщение
            holder.itemView.setOnClickListener {
                onAudioMessage.onPlayMessage(messages[position])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model = messages[position]
        return if (!model.isAudio)
            if(model.isYou) 0 else 1
        else
            if(model.isYou) 2 else 3
    }

    fun addMessage(modelMessage: ModelMessageAdapter){
        messages.add(0, modelMessage)
        notifyItemInserted(0)
    }

    fun setMessages(messages: List<ModelMessageAdapter>){
        this.messages.clear()
        this.messages.addAll(messages)
        notifyDataSetChanged()
    }

    fun clear(){
        val size = messages.size
        messages.clear()
        notifyItemRangeRemoved(0, size)
    }

}
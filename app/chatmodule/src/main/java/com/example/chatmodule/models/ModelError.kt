package com.example.chatmodule.models

data class ModelError(
    val error: String
)

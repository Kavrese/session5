package com.example.chatmodule.models

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

data class ModelAnswerChat <T>(
    val type: String,
    val body: T
)

data class ModelType(
    val type: String
)
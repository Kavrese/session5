package com.example.chatmodule.models

data class ModelDataChat(
    val id: Int,
    val first: ModelUser,
    val second: ModelUser
): java.io.Serializable{
    fun getOtherUser(idUser: Int): ModelUser {
        return if(first.id != idUser) first else second
    }
}

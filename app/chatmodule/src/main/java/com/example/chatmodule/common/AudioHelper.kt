package com.example.chatmodule

import android.media.MediaPlayer

object Audio{
    var nowPlayId: Int? = null
    val audioHelper = AudioHelper()
}

class AudioHelper{
    private val mediaPlayer = MediaPlayer()
    val isPlaying: Boolean
        get() {
            return mediaPlayer.isPlaying
        }
    val duration: Int
        get(){
            return mediaPlayer.duration
        }

    private val completeListeners = mutableListOf<MediaPlayer.OnCompletionListener>()

    init {
        mediaPlayer.setOnPreparedListener {
            mediaPlayer.start()
        }
        mediaPlayer.setOnCompletionListener { player ->
            completeListeners.forEach {
                it.onCompletion(player)
            }
        }
    }

    fun playUrlAudio(url: String){
        mediaPlayer.setDataSource(url)
        mediaPlayer.prepareAsync()
    }

    fun pause(){
        mediaPlayer.pause()
    }

    fun stop(){
        mediaPlayer.stop()
    }

    fun play(){
        mediaPlayer.start()
    }

    fun setProgress(newProgress: Int){
        mediaPlayer.seekTo(newProgress)
    }

    fun getProgress(): Int = mediaPlayer.currentPosition

    fun addOnCompletionListener(onCompleteListener: MediaPlayer.OnCompletionListener){
        completeListeners.add(onCompleteListener)
    }

    fun removeOnCompletionListener(onCompleteListener: MediaPlayer.OnCompletionListener){
        completeListeners.remove(onCompleteListener)
    }
}
package com.example.chatmodule.common.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.chatmodule.models.ModelDataChat
import com.example.chatmodule.databinding.ItemChatBinding
import com.example.chatmodule.common.network.Info

class ChatsAdapter(private var data: List<ModelDataChat> = listOf(), private val onClickChat: OnClickChat): RecyclerView.Adapter<ChatsAdapter.VH>() {
    class VH(private val binding: ItemChatBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(modelChat: ModelDataChat){
            val chatUser = modelChat.getOtherUser(Info.idUser)
            binding.chatName.text = chatUser.getFI()
            binding.chatCoverText.text = chatUser.lastname[0].toString()
            binding.cardChat.setCardBackgroundColor(chatUser.getColorCard())

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
       return VH(ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.setOnClickListener {
            onClickChat.onClickChat(data[position])
        }
        holder.bind(data[position])
    }

    fun setData(data: List<ModelDataChat>){
        this.data = data
        notifyDataSetChanged()
    }
}

interface OnClickChat{
    fun onClickChat(dataChat: ModelDataChat)
}
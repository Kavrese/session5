package com.example.chatmodule.models

data class ModelChat(
    val chat: ModelDataChat,
    val messages: List<ModelMessageChat>
)

